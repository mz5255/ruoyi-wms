package com.ruoyi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableFeignClients
@MapperScan("com.ruoyi.system.mapper")
public class WmsApplication
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
