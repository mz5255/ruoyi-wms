package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CkStashMapper;
import com.ruoyi.system.api.domain.CkStash;
import com.ruoyi.system.service.ICkStashService;

/**
 * 仓库\库区Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class CkStashServiceImpl implements ICkStashService
{
    @Autowired
    private CkStashMapper ckStashMapper;

    /**
     * 查询仓库\库区
     *
     * @param stashNum 仓库\库区主键
     * @return 仓库\库区
     */
    @Override
    public CkStash selectCkStashByStashNum(String stashNum)
    {
        return ckStashMapper.selectCkStashByStashNum(stashNum);
    }

    /**
     * 查询仓库\库区列表
     *
     * @param ckStash 仓库\库区
     * @return 仓库\库区
     */
    @Override
    public List<CkStash> selectCkStashList(CkStash ckStash)
    {
        return ckStashMapper.selectCkStashList(ckStash);
    }

    /**
     * 新增仓库\库区
     *
     * @param ckStash 仓库\库区
     * @return 结果
     */
    @Override
    public int insertCkStash(CkStash ckStash)
    {
        return ckStashMapper.insertCkStash(ckStash);
    }

    /**
     * 修改仓库\库区
     *
     * @param ckStash 仓库\库区
     * @return 结果
     */
    @Override
    public int updateCkStash(CkStash ckStash)
    {
        return ckStashMapper.updateCkStash(ckStash);
    }

    /**
     * 批量删除仓库\库区
     *
     * @param stashNums 需要删除的仓库\库区主键
     * @return 结果
     */
    @Override
    public int deleteCkStashByStashNums(String[] stashNums)
    {
        return ckStashMapper.deleteCkStashByStashNums(stashNums);
    }

    /**
     * 删除仓库\库区信息
     *
     * @param stashNum 仓库\库区主键
     * @return 结果
     */
    @Override
    public int deleteCkStashByStashNum(String stashNum)
    {
        return ckStashMapper.deleteCkStashByStashNum(stashNum);
    }
}
