package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.api.domain.CkMovelog;

/**
 * 移库记录
Service接口
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public interface ICkMovelogService
{
    /**
     * 查询移库记录

     *
     * @param moveNum 移库记录
主键
     * @return 移库记录

     */
    public CkMovelog selectCkMovelogByMoveNum(String moveNum);

    /**
     * 查询移库记录
列表
     *
     * @param ckMovelog 移库记录

     * @return 移库记录
集合
     */
    public List<CkMovelog> selectCkMovelogList(CkMovelog ckMovelog);

    /**
     * 新增移库记录

     *
     * @param ckMovelog 移库记录

     * @return 结果
     */
    public int insertCkMovelog(CkMovelog ckMovelog);

    /**
     * 修改移库记录

     *
     * @param ckMovelog 移库记录

     * @return 结果
     */
    public int updateCkMovelog(CkMovelog ckMovelog);

    /**
     * 批量删除移库记录

     *
     * @param moveNums 需要删除的移库记录
主键集合
     * @return 结果
     */
    public int deleteCkMovelogByMoveNums(String[] moveNums);

    /**
     * 删除移库记录
信息
     *
     * @param moveNum 移库记录
主键
     * @return 结果
     */
    public int deleteCkMovelogByMoveNum(String moveNum);
}
