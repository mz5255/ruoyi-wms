package com.ruoyi.system.controller;

import java.util.List;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.domain.CkMovelog;
import com.ruoyi.system.service.ICkMovelogService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

import javax.servlet.http.HttpServletResponse;

/**
 * 移库记录
Controller
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/movelog")
public class CkMovelogController extends BaseController
{
    @Autowired
    private ICkMovelogService ckMovelogService;

    /**
     * 查询移库记录
列表
     */
    @RequiresPermissions("system:movelog:list")
    @GetMapping("/list")
    public TableDataInfo list(CkMovelog ckMovelog)
    {
        startPage();
        List<CkMovelog> list = ckMovelogService.selectCkMovelogList(ckMovelog);
        return getDataTable(list);
    }

    /**
     * 导出移库记录
列表
     */
    @RequiresPermissions("system:movelog:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkMovelog ckMovelog)
    {
        List<CkMovelog> list = ckMovelogService.selectCkMovelogList(ckMovelog);
        ExcelUtil<CkMovelog> util = new ExcelUtil<CkMovelog>(CkMovelog.class);
        util.exportExcel(response, list, "移库记数据");
    }

    /**
     * 获取移库记录
详细信息
     */
    @RequiresPermissions("system:movelog:query")
    @GetMapping(value = "/{moveNum}")
    public AjaxResult getInfo(@PathVariable("moveNum") String moveNum)
    {
        return success(ckMovelogService.selectCkMovelogByMoveNum(moveNum));
    }

    /**
     * 新增移库记录

     */
    @RequiresPermissions("system:movelog:add")
    @PostMapping
    public AjaxResult add(@RequestBody CkMovelog ckMovelog)
    {
        return toAjax(ckMovelogService.insertCkMovelog(ckMovelog));
    }

    /**
     * 修改移库记录

     */
    @RequiresPermissions("system:movelog:edit")
    @PutMapping
    public AjaxResult edit(@RequestBody CkMovelog ckMovelog)
    {
        return toAjax(ckMovelogService.updateCkMovelog(ckMovelog));
    }

    /**
     * 删除移库记录

     */
    @RequiresPermissions("system:movelog:remove")
	@DeleteMapping("/{moveNums}")
    public AjaxResult remove(@PathVariable String[] moveNums)
    {
        return toAjax(ckMovelogService.deleteCkMovelogByMoveNums(moveNums));
    }
}
