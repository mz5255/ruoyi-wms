package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.domain.CkDeliverylog;
import com.ruoyi.system.service.ICkDeliverylogService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 出库记录Controller
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/deliverylog")
public class CkDeliverylogController extends BaseController
{
    @Autowired
    private ICkDeliverylogService ckDeliverylogService;

    /**
     * 查询出库记录列表
     */
    @RequiresPermissions("system:deliverylog:list")
    @GetMapping("/list")
    public TableDataInfo list(CkDeliverylog ckDeliverylog)
    {
        startPage();
        List<CkDeliverylog> list = ckDeliverylogService.selectCkDeliverylogList(ckDeliverylog);
        return getDataTable(list);
    }

    /**
     * 导出出库记录列表
     */
    @RequiresPermissions("system:deliverylog:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkDeliverylog ckDeliverylog)
    {
        List<CkDeliverylog> list = ckDeliverylogService.selectCkDeliverylogList(ckDeliverylog);
        ExcelUtil<CkDeliverylog> util = new ExcelUtil<CkDeliverylog>(CkDeliverylog.class);
        util.exportExcel(response, list, "出库记录数据");
    }

    /**
     * 获取出库记录详细信息
     */
    @RequiresPermissions("system:deliverylog:query")
    @GetMapping(value = "/{deliveryNum}")
    public AjaxResult getInfo(@PathVariable("deliveryNum") String deliveryNum)
    {
        return success(ckDeliverylogService.selectCkDeliverylogByDeliveryNum(deliveryNum));
    }

    /**
     * 新增出库记录
     */
    @RequiresPermissions("system:deliverylog:add")
    @PostMapping
    public AjaxResult add(@RequestBody CkDeliverylog ckDeliverylog)
    {
        return toAjax(ckDeliverylogService.insertCkDeliverylog(ckDeliverylog));
    }

    /**
     * 修改出库记录
     */
    @RequiresPermissions("system:deliverylog:edit")
    @PutMapping
    public AjaxResult edit(@RequestBody CkDeliverylog ckDeliverylog)
    {
        return toAjax(ckDeliverylogService.updateCkDeliverylog(ckDeliverylog));
    }

    /**
     * 删除出库记录
     */
    @RequiresPermissions("system:deliverylog:remove")
	@DeleteMapping("/{deliveryNums}")
    public AjaxResult remove(@PathVariable String[] deliveryNums)
    {
        return toAjax(ckDeliverylogService.deleteCkDeliverylogByDeliveryNums(deliveryNums));
    }
}
