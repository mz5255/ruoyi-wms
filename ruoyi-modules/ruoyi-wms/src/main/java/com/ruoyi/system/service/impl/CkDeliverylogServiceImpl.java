package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CkDeliverylogMapper;
import com.ruoyi.system.api.domain.CkDeliverylog;
import com.ruoyi.system.service.ICkDeliverylogService;

/**
 * 出库记录Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class CkDeliverylogServiceImpl implements ICkDeliverylogService
{
    @Autowired
    private CkDeliverylogMapper ckDeliverylogMapper;

    /**
     * 查询出库记录
     *
     * @param deliveryNum 出库记录主键
     * @return 出库记录
     */
    @Override
    public CkDeliverylog selectCkDeliverylogByDeliveryNum(String deliveryNum)
    {
        return ckDeliverylogMapper.selectCkDeliverylogByDeliveryNum(deliveryNum);
    }

    /**
     * 查询出库记录列表
     *
     * @param ckDeliverylog 出库记录
     * @return 出库记录
     */
    @Override
    public List<CkDeliverylog> selectCkDeliverylogList(CkDeliverylog ckDeliverylog)
    {
        return ckDeliverylogMapper.selectCkDeliverylogList(ckDeliverylog);
    }

    /**
     * 新增出库记录
     *
     * @param ckDeliverylog 出库记录
     * @return 结果
     */
    @Override
    public int insertCkDeliverylog(CkDeliverylog ckDeliverylog)
    {
        return ckDeliverylogMapper.insertCkDeliverylog(ckDeliverylog);
    }

    /**
     * 修改出库记录
     *
     * @param ckDeliverylog 出库记录
     * @return 结果
     */
    @Override
    public int updateCkDeliverylog(CkDeliverylog ckDeliverylog)
    {
        return ckDeliverylogMapper.updateCkDeliverylog(ckDeliverylog);
    }

    /**
     * 批量删除出库记录
     *
     * @param deliveryNums 需要删除的出库记录主键
     * @return 结果
     */
    @Override
    public int deleteCkDeliverylogByDeliveryNums(String[] deliveryNums)
    {
        return ckDeliverylogMapper.deleteCkDeliverylogByDeliveryNums(deliveryNums);
    }

    /**
     * 删除出库记录信息
     *
     * @param deliveryNum 出库记录主键
     * @return 结果
     */
    @Override
    public int deleteCkDeliverylogByDeliveryNum(String deliveryNum)
    {
        return ckDeliverylogMapper.deleteCkDeliverylogByDeliveryNum(deliveryNum);
    }
}
