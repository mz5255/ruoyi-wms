package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.domain.CkStash;
import com.ruoyi.system.service.ICkStashService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 仓库\库区Controller
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/stash")
public class CkStashController extends BaseController
{
    @Autowired
    private ICkStashService ckStashService;

    /**
     * 查询仓库\库区列表
     */
    @RequiresPermissions("system:stash:list")
    @GetMapping("/list")
    public TableDataInfo list(CkStash ckStash)
    {
        startPage();
        List<CkStash> list = ckStashService.selectCkStashList(ckStash);
        return getDataTable(list);
    }

    /**
     * 导出仓库\库区列表
     */
    @RequiresPermissions("system:stash:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkStash ckStash)
    {
        List<CkStash> list = ckStashService.selectCkStashList(ckStash);
        ExcelUtil<CkStash> util = new ExcelUtil<CkStash>(CkStash.class);
        util.exportExcel(response, list, "仓库or库区数据");
    }

    /**
     * 获取仓库\库区详细信息
     */
    @RequiresPermissions("system:stash:query")
    public AjaxResult getInfo(@PathVariable("stashNum") String stashNum)
    {
        return success(ckStashService.selectCkStashByStashNum(stashNum));
    }

    /**
     * 新增仓库\库区
     */
    @RequiresPermissions("system:stash:add")
    @PostMapping
    public AjaxResult add(@RequestBody CkStash ckStash)
    {
        return toAjax(ckStashService.insertCkStash(ckStash));
    }

    /**
     * 修改仓库\库区
     */
    @RequiresPermissions("system:stash:edit")
    @PutMapping
    public AjaxResult edit(@RequestBody CkStash ckStash)
    {
        return toAjax(ckStashService.updateCkStash(ckStash));
    }

    /**
     * 删除仓库\库区
     */
    @RequiresPermissions("system:stash:remove")
	@DeleteMapping("/{stashNums}")
    public AjaxResult remove(@PathVariable String[] stashNums)
    {
        return toAjax(ckStashService.deleteCkStashByStashNums(stashNums));
    }
}
