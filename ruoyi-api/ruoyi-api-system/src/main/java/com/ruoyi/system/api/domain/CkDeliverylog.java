package com.ruoyi.system.api.domain;

import java.math.BigDecimal;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 出库记录对象 ck_deliverylog
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Data
public class CkDeliverylog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 出库编号 */
    @Excel(name = "出库编号")
    private String deliveryNum;

    /** 出库类型 */
    @Excel(name = "出库类型")
    private Long deliveryType;

    /** 顾客名称 */
    @Excel(name = "顾客名称")
    private String clinetName;

    /** 物流编号 */
    @Excel(name = "物流编号")
    private String logisticsNum;

    /** 状态（0 未发货 1部分发货  2已发货 3作废） */
    @Excel(name = "状态", readConverterExp = "0=,未=发货,1=部分发货,2=已发货,3=作废")
    private String status;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String goodsNum;

    /** 源仓库编号 */
    @Excel(name = "源仓库编号")
    private String sourceNum;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal money;

    /** 删除标识（ 0正常 1代表已删除） */
    private String delFlag;


}
