package com.ruoyi.system.api.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 仓库\库区对象 ck_stash
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Data
public class CkStash extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库\库区编号 */
    @Excel(name = "仓库or库区编号")
    private String stashNum;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String stashName;

    /** 仓库上级编号 */
    @Excel(name = "仓库上级编号")
    private String stashParentid;


}
