package com.ruoyi.system.api.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 移库记录
对象 ck_movelog
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Data
public class CkMovelog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 移库记录编号 */
    @Excel(name = "移库记录编号")
    private String moveNum;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String goodsNum;

    /** 数量 */
    @Excel(name = "数量")
    private String amout;

    /** 源库编号 */
    @Excel(name = "源库编号")
    private String sourceNum;

    /** 目标库编号 */
    @Excel(name = "目标库编号")
    private String targetNum;

    /** 状态（ 0代表未操作 1代表部分移动 2代表移动完毕 3代表作废） */
    @Excel(name = "状态", readConverterExp = "0=代表未操作,1=代表部分移动,2=代表移动完毕,3=代表作废")
    private String status;

    /** 删除标识（0正常  1已删除） */
    private String delFlag;


}
