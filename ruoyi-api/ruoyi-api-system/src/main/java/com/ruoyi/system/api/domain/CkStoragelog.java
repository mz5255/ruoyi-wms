package com.ruoyi.system.api.domain;

import java.math.BigDecimal;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 入库记录对象 ck_storagelog
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Data
public class CkStoragelog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 入库编号 */
    @Excel(name = "入库编号")
    private String storageNum;

    /** 入库类型 */
    @Excel(name = "入库类型")
    private Long storageType;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supplier;

    /** 物流编号 */
    @Excel(name = "物流编号")
    private String logisticsNum;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String goodsNum;

    /** 目标仓库 */
    @Excel(name = "目标仓库")
    private String targetNum;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal money;

    /** 删除标识（0代表正常  1代表删除） */
    private String delFlag;


}
