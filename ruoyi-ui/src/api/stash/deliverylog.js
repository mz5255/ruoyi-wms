import request from '@/utils/request'

// 查询出库记录列表
export function listDeliverylog(query) {
  return request({
    url: '/system/deliverylog/list',
    method: 'get',
    params: query
  })
}

// 查询出库记录详细
export function getDeliverylog(deliveryNum) {
  return request({
    url: '/system/deliverylog/' + deliveryNum,
    method: 'get'
  })
}

// 新增出库记录
export function addDeliverylog(data) {
  return request({
    url: '/system/deliverylog',
    method: 'post',
    data: data
  })
}

// 修改出库记录
export function updateDeliverylog(data) {
  return request({
    url: '/system/deliverylog',
    method: 'put',
    data: data
  })
}

// 删除出库记录
export function delDeliverylog(deliveryNum) {
  return request({
    url: '/system/deliverylog/' + deliveryNum,
    method: 'delete'
  })
}
